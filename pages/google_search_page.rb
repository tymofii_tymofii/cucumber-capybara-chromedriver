class GoogleSearchPage < BasePage
  set_url CommonVarsHelper::TEST_APPLICATION_HOST
  element :search_input, '#lst-ib'
  element :search_button, '[name="btnK"]'

  def search(search_key)
    search_input.set(search_key)
    search_input.native.send_keys(:return)
  end
end
