class GoogleResultsPage < BasePage

  def results_urls
    all('._Rm').map(&:text)
  end
end
