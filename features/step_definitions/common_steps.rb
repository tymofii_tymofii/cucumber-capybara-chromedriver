When(/^I open (.*)$/) do |page_to_open|
  page_instance = Object.const_get(page_to_open).new
  page_instance.open
end

When(/^perform search for "(.*)"$/) do |search_string|
  web_app.google_search_page.search(search_string)
end

Then("the results list should include {string}") do |string|
  expect(web_app.google_results_page.results_urls).to include(string)
end
